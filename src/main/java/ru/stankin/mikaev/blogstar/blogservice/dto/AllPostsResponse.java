package ru.stankin.mikaev.blogstar.blogservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

/**
 * AllPostsResponse.
 *
 * @author Nikita_Mikaev
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AllPostsResponse {
    private ProfileInfoDto owner;

    private List<PostResponse> posts;
}
