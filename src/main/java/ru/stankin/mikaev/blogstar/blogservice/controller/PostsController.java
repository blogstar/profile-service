package ru.stankin.mikaev.blogstar.blogservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.stankin.mikaev.blogstar.blogservice.dto.AllPostsResponse;
import ru.stankin.mikaev.blogstar.blogservice.dto.PostResponse;
import ru.stankin.mikaev.blogstar.blogservice.dto.SavePostRequest;
import ru.stankin.mikaev.blogstar.blogservice.service.PostsService;
import java.util.List;
import java.util.Optional;

/**
 * BlogController.
 *
 * @author Nikita_Mikaev
 */
@RestController
@RequestMapping("/api/v1/blog")
@PreAuthorize("isAuthenticated()")
@RequiredArgsConstructor
public class PostsController {

    private final PostsService postsService;

    @PostMapping("/posts")
    public Long createPost(@RequestBody SavePostRequest post) {
        return postsService.createPost(post);
    }

    @GetMapping("/posts")
    public AllPostsResponse getPosts() {
        return postsService.getUserPosts();
    }

    @GetMapping("/posts/{id}")
    public ResponseEntity<PostResponse> getPost(@PathVariable("id") Long id) {
        Optional<PostResponse> post = postsService.getPost(id);
        return post.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }
}
