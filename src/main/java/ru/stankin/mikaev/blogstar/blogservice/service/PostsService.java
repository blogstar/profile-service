package ru.stankin.mikaev.blogstar.blogservice.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.stankin.mikaev.blogstar.blogservice.client.ProfileServiceClient;
import ru.stankin.mikaev.blogstar.blogservice.dto.AllPostsResponse;
import ru.stankin.mikaev.blogstar.blogservice.dto.PostResponse;
import ru.stankin.mikaev.blogstar.blogservice.dto.SavePostRequest;
import ru.stankin.mikaev.blogstar.blogservice.mapper.PostMapper;
import ru.stankin.mikaev.blogstar.blogservice.model.Post;
import ru.stankin.mikaev.blogstar.blogservice.repository.PostsRepository;
import java.util.List;
import java.util.Optional;

/**
 * PostsService.
 *
 * @author Nikita_Mikaev
 */
@Service
@RequiredArgsConstructor
public class PostsService {

    private final PostsRepository postsRepository;

    private final ProfileServiceClient profileServiceClient;

    public Long createPost(SavePostRequest post) {
        Post postEntity = PostMapper.map(post);
        postsRepository.save(postEntity);
        return postEntity.getId();
    }

    public AllPostsResponse getUserPosts() {
        String userId = SecurityService.getUserId();
        List<PostResponse> userPosts = PostMapper.mapMany(postsRepository.findAllByOwnerId(userId));
        return AllPostsResponse.builder()
                .owner(profileServiceClient.getUserInfo(userId))
                .posts(userPosts)
                .build();
    }

    public Optional<PostResponse> getPost(Long id) {
        PostResponse postDto = null;
        Optional<Post> byId = postsRepository.findById(id);
        if (byId.isPresent()) {
            postDto = PostMapper.map(byId.get());
        }
        return Optional.ofNullable(postDto);
    }
}
