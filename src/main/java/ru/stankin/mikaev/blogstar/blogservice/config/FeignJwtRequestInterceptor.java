package ru.stankin.mikaev.blogstar.blogservice.config;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import ru.stankin.mikaev.blogstar.blogservice.service.SecurityService;

/**
 * FeignJwtRequestInterceptor.
 *
 * @author Nikita_Mikaev
 */
public class FeignJwtRequestInterceptor implements RequestInterceptor {

    private static final String BEARER_HEADER = "Bearer ";

    @Override
    public void apply(RequestTemplate requestTemplate) {
        requestTemplate.header("Authorization", BEARER_HEADER + SecurityService.getJwtToken());
    }
}