package ru.stankin.mikaev.blogstar.blogservice.repository;

import org.springframework.data.repository.CrudRepository;
import ru.stankin.mikaev.blogstar.blogservice.model.Post;
import java.util.List;

/**
 * PostsRepository.
 *
 * @author Nikita_Mikaev
 */
public interface PostsRepository extends CrudRepository<Post, Long> {

    List<Post> findAllByOwnerId(String ownerId);
}
