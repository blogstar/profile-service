package ru.stankin.mikaev.blogstar.blogservice.constants;

/**
 * ServiceConstants.
 *
 * @author Nikita_Mikaev
 */
public class ServiceConstants {
    public static final String DEFAULT_PROFILE = "default";
    public static final String TEST_PROFILE = "test";
    public static final String LOCAL_PROFILE = "local";
    private ServiceConstants() {

    }
}
