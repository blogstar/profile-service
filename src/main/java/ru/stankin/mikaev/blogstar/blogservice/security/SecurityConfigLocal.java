package ru.stankin.mikaev.blogstar.blogservice.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import static ru.stankin.mikaev.blogstar.blogservice.constants.ServiceConstants.TEST_PROFILE;

/**
 * SecurityConfigLocal.
 *
 * @author Nikita_Mikaev
 */
@Configuration
@EnableWebSecurity
@Profile({TEST_PROFILE})
@Slf4j
public class SecurityConfigLocal extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        log.info("Run TEST Security Configuration");
    }
}