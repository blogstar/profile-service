package ru.stankin.mikaev.blogstar.blogservice.service;

import lombok.experimental.UtilityClass;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * SecurityService.
 *
 * @author Nikita_Mikaev
 */
@UtilityClass
public class SecurityService {

    private static KeycloakPrincipal<KeycloakSecurityContext> getKeycloakPrincipal() {
        KeycloakAuthenticationToken authentication = (KeycloakAuthenticationToken) SecurityContextHolder.getContext()
                .getAuthentication();

        return (KeycloakPrincipal<KeycloakSecurityContext>) authentication.getPrincipal();
    }

    public static String getJwtToken() {
        var keycloakPrincipal = getKeycloakPrincipal();
        return keycloakPrincipal.getKeycloakSecurityContext().getTokenString();
    }

    public static String getUserId() {
        var keycloakPrincipal = getKeycloakPrincipal();
        return (String) keycloakPrincipal.getKeycloakSecurityContext().getToken().getOtherClaims().get("user_id");
    }

}