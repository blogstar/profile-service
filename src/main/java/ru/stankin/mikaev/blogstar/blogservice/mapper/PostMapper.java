package ru.stankin.mikaev.blogstar.blogservice.mapper;

import static java.util.stream.Collectors.toList;

import ru.stankin.mikaev.blogstar.blogservice.dto.PostResponse;
import ru.stankin.mikaev.blogstar.blogservice.dto.SavePostRequest;
import ru.stankin.mikaev.blogstar.blogservice.model.Post;
import ru.stankin.mikaev.blogstar.blogservice.service.SecurityService;
import java.util.List;

/**
 * PostMapper.
 *
 * @author Nikita_Mikaev
 */
public class PostMapper {

    public static PostResponse map(Post post) {
        return PostResponse.builder()
                .id(post.getId())
                .subject(post.getSubject())
                .content(post.getContent())
                .dateTime(post.getDateTime())
                .build();
    }

    public static List<PostResponse> mapMany(List<Post> posts) {
        return posts.stream().map(PostMapper::map).collect(toList());
    }

    public static Post map(SavePostRequest post) {
        return Post.builder()
                .ownerId(SecurityService.getUserId())
                .subject(post.getSubject())
                .content(post.getContent())
                .build();
    }
}
