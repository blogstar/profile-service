package ru.stankin.mikaev.blogstar.blogservice.config;

import feign.RequestInterceptor;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * FeignConfig.
 *
 * @author Nikita_Mikaev
 */
@EnableFeignClients(basePackages = "ru.stankin.mikaev.blogstar.blogservice.client")
@Configuration
public class FeignConfig {

    @Bean
    RequestInterceptor requestInterceptor() {
        return new FeignJwtRequestInterceptor();
    }
}
