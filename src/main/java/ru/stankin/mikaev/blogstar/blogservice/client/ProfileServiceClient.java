package ru.stankin.mikaev.blogstar.blogservice.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.stankin.mikaev.blogstar.blogservice.config.FeignConfig;
import ru.stankin.mikaev.blogstar.blogservice.dto.ProfileInfoDto;

/**
 * BlogServiceClient.
 *
 * @author Nikita_Mikaev
 */
@FeignClient(name = "profile-service", configuration = FeignConfig.class)
public interface ProfileServiceClient {

    @RequestMapping(path = "/api/v1/profile/info", method = RequestMethod.GET)
    ProfileInfoDto getUserInfo(String userId);
}
